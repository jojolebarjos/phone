
from setuptools import find_packages, setup


with open("README.md", "r", encoding="utf-8") as file:
    long_description = file.read()

setup(

    name = "phone",
    version = "0.0.1",
    packages = find_packages(),

    author = "Johan Berdat",
    author_email = "jojolebarjos@gmail.com",
    license = "MIT",

    url = "https://gitlab.com/jojolebarjos/phone",
    project_urls = {
        "Tracker": "https://gitlab.com/jojolebarjos/phone/-/issues",
    },

    description = "Grapheme to phoneme",
    long_description = long_description,
    long_description_content_type = "text/markdown",

    keywords = [
        "grapheme",
        "phoneme",
        "ipa",
    ],

    classifiers = [
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Linguistic",
        "Topic :: Utilities",
    ],

    include_package_data = True,

    python_requires = ">=3.6",
    install_requires = [
        "torch>=1.5.1",
    ],

)
