
# Wiktionary IPA

_See [this repository](https://gitlab.com/jojolebarjos/wiktionary-ipa) for the original extraction._

The following code was used to generate the dataset:

```python
import pandas as pd

# Load original dataset
df = pd.read_csv("fr.tsv", sep="\t", na_filter=False)

# Discard upper combination breve
df["pronunciation"] = df["pronunciation"].str.replace("\u0361", "")

# Discard language column, keep French only
df = df[df["language"] == "fr"]
df = df[["text", "pronunciation"]]

# Get all items with at least one space character
mask = df["text"].str.contains(" ")
space_df = df[mask]
no_space_df = df[~mask]

# Also keep some without any space character
balanced_df = pd.concat([
    space_df,
    no_space_df.sample(n=len(space_df)*3),
], axis=0)

# Shuffle and split
balanced_df = balanced_df.sample(frac=1)
n = len(balanced_df) // 10
test_df = balanced_df.iloc[:3*n]
validation_df = balanced_df.iloc[3*n:4*n]
train_df = balanced_df.iloc[4*n:]

# Save
train_df.to_csv("fr.train.tsv", index=False, sep="\t", encoding="utf-8", line_terminator="\n")
validation_df.to_csv("fr.validation.tsv", index=False, sep="\t", encoding="utf-8", line_terminator="\n")
test_df.to_csv("fr.test.tsv", index=False, sep="\t", encoding="utf-8", line_terminator="\n")
```

As per [Wikipedia policy](https://en.wikipedia.org/wiki/Wikipedia:Reusing_Wikipedia_content), this dataset is released under the _CC-BY 4.0_ license.
