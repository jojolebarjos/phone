
import os

import regex as re

import torch
import torch.nn as nn
import torch.nn.functional as F

from .util import HERE, PHONEMES, PHONEME_MAP
from .sampa import tokenize_ipa


ORAL_VOWELS = {
    "i", # si
    "e", # ces
    "ɛ", # seize
    "a", # patte
    "ɑ", # pâte
    "ɔ", # comme
    "o", # gros
    "u", # doux
    "y", # du
    "ø", # deux
    "œ", # neuf
    "ə", # justement
}

NASAL_VOWELS = {
    "ɛ̃", # vin
    "ɑ̃", # vent
    "ɔ̃", # bon
    "œ̃", # brun
}

VOWELS = ORAL_VOWELS.union(NASAL_VOWELS)


# Convert raw data to tensors
def encode(v):
    phonemes = []
    split_after = []
    is_vowel = []
    for p in tokenize_ipa(v):
        
        # Dot after is a syllable marker, and is stripped
        if p == ".":
            split_after[-1] = 1
            continue
        
        # Liaison is not a syllable marker, and is converted to a space
        s = False
        if p == "‿":
            p = " "
        
        # Space is a syllable marker
        elif p == " ":
            s = True
        
        # Append character
        phonemes.append(PHONEME_MAP[p])
        split_after.append(s)
        is_vowel.append(p in VOWELS)

    # Pack as tensors
    phonemes = torch.tensor(phonemes)
    split_after = torch.tensor(split_after)
    is_vowel = torch.tensor(is_vowel)
    
    return phonemes, split_after, is_vowel


def sum_block(x, m):
    """Sum each block along axis 0, where m marks new block."""
    
    # Invert mask
    m = ~m
    
    # Forward exclusive cumsum, with reset
    acc = torch.zeros_like(x[0])
    forward = []
    for i in range(x.shape[0]):
        acc = acc * m[i]
        forward.append(acc)
        acc = acc + x[i]
    forward = torch.stack(forward)

    # Backward inclusive cumsum, with reset
    acc = torch.zeros_like(x[0])
    backward = []
    for i in reversed(range(x.shape[0])):
        acc = acc + x[i]
        backward.append(acc)
        acc = acc * m[i]
    backward.reverse()
    backward = torch.stack(backward)
    
    return forward + backward


# As defined in training notebook
class Model(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.num_symbol = len(PHONEMES)
        self.embedding_size = 16
        self.hidden_size = 32
        
        # Embed characters
        self.embedding = nn.Embedding(
            num_embeddings=self.num_symbol,
            embedding_dim=self.embedding_size,
            padding_idx=0,
        )
        
        # Use a single GRU
        self.gru = nn.GRU(
            input_size=self.embedding_size,
            hidden_size=self.hidden_size,
            num_layers=1,
            dropout=0,
            bidirectional=True,
        )
        
        # Use two separate classifiers
        self.classifier = nn.Linear(self.hidden_size * 2, 1)
    
    def _encode(self, phonemes, lengths):
        embeddings = self.embedding(phonemes)
        packed_embeddings = nn.utils.rnn.pack_padded_sequence(embeddings, lengths, enforce_sorted=False)
        packed_encodings, _ = self.gru(packed_embeddings)
        encodings, _ = nn.utils.rnn.pad_packed_sequence(packed_encodings)
        logits = self.classifier(encodings).squeeze(-1)
        return logits
    
    def forward(self, phonemes, is_vowel, split_after, lengths):
        
        # Apply model
        logits = self._encode(phonemes, lengths)
        
        # Compute exponentials
        x = torch.exp(logits) # TODO subtract min?

        # Ignore before first vowel and after last vowel
        mask = is_vowel.cumsum(dim=0)
        mask = (mask > 0) & (mask < mask[-1])

        # Compute normalization sums, for each block
        denom = sum_block(x, is_vowel)

        # Finally, compute softmax
        probabilities = x / denom

        # Get sum of losses, excluding first and last block
        losses = F.binary_cross_entropy(probabilities, split_after.to(torch.float32), reduction="none")
        loss = (losses * mask).sum(dim=0).mean()
        return loss


# Load pretrained model
model = Model()
path = os.path.join(HERE, "syllable.pt")
model.load_state_dict(torch.load(path))
model.eval()


def segment(pronunciation):
    """Annotate syllables and liaisons, assuming French."""

    # Encode
    phonemes, split_after, is_vowel = encode(pronunciation)

    # Apply model
    logits = model._encode(phonemes.unsqueeze(1), torch.tensor(phonemes.shape[0]).unsqueeze(0)).squeeze(1)

    # Reconstruct pronunciation
    offsets = torch.arange(is_vowel.shape[0])[is_vowel]
    parts = []
    for o in range(offsets[0]):
        parts.append(PHONEMES[phonemes[o]])
    for i, j in zip(offsets[:-1], offsets[1:]):
        k = i + logits[i:j].argmax()
        for o in range(i, k+1):
            parts.append(PHONEMES[phonemes[o]])
        parts.append(".")
        for o in range(k+1, j):
            parts.append(PHONEMES[phonemes[o]])
    for o in range(offsets[-1], phonemes.shape[0]):
        parts.append(PHONEMES[phonemes[o]])
    syllable = "".join(parts)
    syllable = syllable.replace(" .", "<>")
    syllable = syllable.replace(". ", "<>")
    syllable = syllable.replace(" ", "‿")
    syllable = syllable.replace("<>", " ")
    return syllable
