
import os

import regex as re

import torch
import torch.nn as nn
import torch.nn.functional as F

from .util import HERE, GRAPHEMES, GRAPHEME_MAP, PHONEMES
from .util import normalize, simplify


# As defined in training notebook
class Model(nn.Module):
    def __init__(self, num_input_symbols, num_output_symbols, embedding_size, hidden_size):
        super().__init__()
        
        # Store parameters
        self.num_input_symbols = num_input_symbols
        self.num_output_symbols = num_output_symbols
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size
        
        # Input symbols are embedded
        self.embedding = nn.Embedding(
            num_embeddings=num_input_symbols,
            embedding_dim=embedding_size,
            padding_idx=0,
        )
        
        # We will use a combination of regular convolutions and transposed convolutions
        # Note that we need some expansion, as CTC assumes that output lengths are smaller than input lengths
        self.c1 = nn.Conv1d(embedding_size, hidden_size, kernel_size=3, padding=1)
        self.c2 = nn.Conv1d(hidden_size, hidden_size * 2, kernel_size=3, padding=1)
        self.c3 = nn.ConvTranspose1d(hidden_size * 2, hidden_size * 3, kernel_size=3, stride=3)
        self.c4 = nn.Conv1d(hidden_size * 3, hidden_size * 4, kernel_size=3, padding=1)
        self.c5 = nn.Conv1d(hidden_size * 4, num_output_symbols, kernel_size=1)
    
    def forward(self, input_sequences, input_lengths):
        
        # First, embed symbols
        tmp = self.embedding(input_sequences)
        
        # Need to transpose, as
        #   rnn are length x batch_size x features
        #   convolution are batch_size x features x length
        tmp = tmp.permute(1, 2, 0)
        
        # Apply convolutions
        mask = torch.arange(tmp.shape[2], device=tmp.device)[None, None, :] < input_lengths[:, None, None]
        tmp = F.leaky_relu(self.c1(tmp * mask))
        tmp = F.leaky_relu(self.c2(tmp * mask))
        
        # Upsample
        tmp = F.leaky_relu(self.c3(tmp * mask))
        input_lengths = input_lengths * 3
        
        # Apply convolutions
        mask = torch.arange(tmp.shape[2], device=tmp.device)[None, None, :] < input_lengths[:, None, None]
        tmp = F.leaky_relu(self.c4(tmp * mask))
        tmp = self.c5(tmp * mask)
        
        # Restore dimension order
        output_sequences = tmp.permute(2, 0, 1)
        
        # Compute new length (hard-coded, depends on our convolutions)
        output_lengths = input_lengths * 3
        
        return output_sequences, input_lengths


# Load pretrained model
path = os.path.join(HERE, "phoneme.pt")
model = Model(
    num_input_symbols=len(GRAPHEMES),
    num_output_symbols=len(PHONEMES),
    embedding_size=16,
    hidden_size=64,
)
model.load_state_dict(torch.load(path))
model.eval()


def translate(text):
    """Convert graphemes to phonemes, assuming French."""

    with torch.no_grad():

        # Prepare text
        text = normalize(text)
        text = simplify(text)

        # Convert to tensor
        indices = torch.zeros(len(text), dtype=torch.long)
        for i, c in enumerate(text):
            indices[i] = GRAPHEME_MAP[c]

        # Apply model
        sequences = indices.unsqueeze(1)
        lengths = torch.tensor([sequences.shape[0]], dtype=torch.long)
        logits, lengths = model(sequences, lengths)

        # Extract the raw indices (i.e. best path decoding)
        indices = logits.argmax(dim=2)
        length = lengths[0]
        indices = indices[:length, 0].tolist()

        # Deduplicate indices, as per CTC scheme
        output = []
        previous = None
        for index in indices:
            if index != previous:
                previous = index
                if index > 0:
                    output.append(index)

        # Convert back to IPA
        pronunciation = "".join(PHONEMES[i] for i in output if i != 0)
        
        # Clean whitespace
        pronunciation = re.sub(r"\ +", " ", pronunciation).strip()

        return pronunciation
