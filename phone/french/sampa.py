
import regex as re


# https://en.wikipedia.org/wiki/SAMPA_chart
SAMPA_TO_IPA = {

    # Punctuation
    " ": " ",
    ".": ".",
    "_": "‿",

    # Plosives consonants
    "p": "p", # pont
    "b": "b", # bon
    "t": "t", # temps
    "d": "d", # dans
    "k": "k", # quand
    "g": "ɡ", # gant

    # Voiceless plosives consonants
    "f": "f", # femme
    "v": "v", # vent
    "s": "s", # sans
    "z": "z", # zone
    "S": "ʃ", # champ
    "Z": "ʒ", # gens
    "j": "j", # ion

    # Nasals consonants
    "m": "m", # mont
    "n": "n", # nom
    "J": "ŋ", # oignon
    "N": "ɲ", # camping

    # Liquids consonants
    "l": "l", # long
    "R": "ʁ", # rond

    # Vowel glides
    "w": "w", # coin
    "H": "ɥ", # juin
    "j": "j", # pierre

    # Oral vowels
    "i": "i", # si
    "I": "ɪ", # bit (en), used in Québec French
    "e": "e", # ces
    "E": "ɛ", # seize
    "3": "ɜ", # bird (en), used in Québec French
    "a": "a", # patte
    "A": "ɑ", # pâte
    "O": "ɔ", # comme
    "o": "o", # gros
    "u": "u", # doux
    "U": "ʊ", # hook (en), used in Québec French
    "y": "y", # du
    "Y": "ʏ", # hübsch (de), used in Québec French
    "2": "ø", # deux
    "9": "œ", # neuf
    "@": "ə", # justement

    # Nasal vowels
    "E~": "ɛ̃", # vin
    "A~": "ɑ̃", # vent
    "O~": "ɔ̃", # bon
    "9~": "œ̃", # brun
}
IPA_TO_SAMPA = {b: a for a, b in SAMPA_TO_IPA.items()}


# Character splitter regexes
SAMPA_REGEX = re.compile(r"(.~?)*")
IPA_REGEX = re.compile(r"(.\u0303?\u02d0?)*")


def tokenize_ipa(pronunciation):
    """Split IPA phonemes."""

    return IPA_REGEX.fullmatch(pronunciation).captures(1)


def tokenize_sampa(pronunciation):
    """Split SAMPA phonemes"""

    return SAMPA_REGEX.fullmatch(pronunciation).captures(1)


def sampa_to_ipa(pronunciation):
    """Convert from French SAMPA to IPA."""

    return "".join(SAMPA_TO_IPA[p] for p in tokenize_sampa(pronunciation))


def ipa_to_sampa(pronunciation):
    """Convert from IPA to French SAMPA."""

    return "".join(IPA_TO_SAMPA[p] for p in tokenize_ipa(pronunciation))
