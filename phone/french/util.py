
import os
import unicodedata

import regex as re


# Local folder
HERE = os.path.dirname(__file__)


# Characters accepted in French (simplified for phonetic usage)
GRAPHEMES = ["<PAD>"] + list(" .'-abcdefghijklmnopqrstuvwxyzàâäçèéêëîïôöùûü")
GRAPHEME_MAP = {c: i for i, c in enumerate(GRAPHEMES)}

# IPA symbols used in French
PHONEMES = [
    "<BLANK>",
    " ", "a", "b", "d", "e", "f", "i", "j",
    "k", "l", "m", "n", "o", "p", "s", "t",
    "u", "v", "w", "y", "z", "ø", "ŋ", "œ",
    "œ̃", "ɑ", "ɑ̃", "ɔ", "ɔ̃", "ə", "ɛ", "ɛ̃",
    "ɡ", "ɥ", "ɲ", "ʁ", "ʃ", "ʒ",
]
PHONEME_MAP = {c: i for i, c in enumerate(PHONEMES)}


def normalize(text):
    """Normalize characters and punctuation for French."""

    # Convert to decomposed normal form, and compose characters again
    text = unicodedata.normalize("NFC", text)

    # TODO unidecode-based simplification, possibly using custom mappings for French
    # See https://github.com/jojolebarjos/wikipedia-text/blob/master/clean.py

    return text


def simplify(text):
    """Simplify characters and punctuation for French pronunciation."""

    # Casing is not important
    text = text.lower()

    # Simplify characters
    text = re.sub(r"[\"]+", "", text)
    text = re.sub(r"\s*\n\s*", ".", text)
    text = re.sub(r"\s+", " ", text)
    text = re.sub(r"[\,\:\;\?\!\(\)\.]+\ *", ".", text)
    
    # Make sure start and end is marked using a dot
    if not text.startswith("."):
        text = "." + text
    if not text.endswith("."):
        text = text + "."

    return text
