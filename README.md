
# Phonetic Transcription

In this project, we experiment phonetic transcription of plain text.


## Getting started

Install using the following command (you will need to install [PyTorch](https://pytorch.org/) first):

```
pip install git+https://gitlab.com/jojolebarjos/phone.git
```

High-level methods are provided to translate and segment plain text into IPA:

```python
from phone.french import translate, segment

segment(translate("Je ne sais pas ce que cela veut dire..."))
# -> "ʒə nə sɛ pa sə kə sə.la vø diʁ"
```

Some helpers are also provided if you prefer ASCII-based notations:

```python
from phone.french import ipa_to_sampa, sampa_to_ipa

ipa_to_sampa("a.vɛk de.z\u203fa.mi")
# -> "a.vEk de.z_a.mi"

sampa_to_ipa("l@ Si.E~ A~.tER sO~.n_Os")
# -> "lə ʃi.ɛ̃ ɑ̃.tɛʁ sɔ̃.n‿ɔs"
```


## Links and References

 * ...
