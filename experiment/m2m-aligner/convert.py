
import pandas as pd

import regex as re

import csv


# Load training set
df = pd.read_csv("../../data/wiktionary/fr.train.tsv", sep="\t", na_filter=False)

# Ignore case and replace spaces with special symbol, as space is used by m2m-aligner
def prepare_text(text):
    text = text.lower()
    text = text.replace(" ", "_")
    text = " ".join(text)
    return text

# Liaisons are considered as whitespace
BLANK_R = re.compile(r"[ \u203f]")

# Some IPA annotations are ignored
STRIP_R = re.compile(r"[\(\)\u0361h\u0294]")

# IPA tokenization regex
IPA_R = re.compile(r"(.\u0303?\u02d0?)*")

# Prepare pronunciation for inference
def prepare_pronunciation(pronunciation):
    pronunciation = BLANK_R.sub("_", pronunciation)
    pronunciation = STRIP_R.sub("", pronunciation)
    match = IPA_R.fullmatch(pronunciation)
    pronunciation = " ".join(match.captures(1))
    return pronunciation

# Strip dots
SIMPLE_R = re.compile(r"\. ?")

# Strip syllable separators for training
def simplify_pronunciation(pronunciation):
    pronunciation = SIMPLE_R.sub("", pronunciation)
    return pronunciation

# Apply preprocessing
df["prepared_text"] = df["text"].apply(prepare_text)
df["prepared_pronunciation"] = df["pronunciation"].apply(prepare_pronunciation)
df["simple_pronunciation"] = df["prepared_pronunciation"].apply(simplify_pronunciation)

# Export whole dataset, for further processing
df.to_csv("fr.tsv", sep="\t", encoding="utf-8", index=False)

# Keep only preprocessed columns, for m2m-aligner
with open("infer.tsv", "w", encoding="utf-8", newline="\n") as file:
    for _, row in df.iterrows():
        file.write(f"{row.prepared_text}\t{row.simple_pronunciation}\n")

# Keep a smaller training set, to go easy on m2m-aligner
train_df = infer_df.sample(n=30000)
with open("train.tsv", "w", encoding="utf-8", newline="\n") as file:
    for _, row in train_df.iterrows():
        file.write(f"{row.prepared_text}\t{row.simple_pronunciation}\n")
