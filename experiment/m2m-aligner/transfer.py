
import pandas as pd

from tqdm import tqdm

# Load full dataset
df = pd.read_csv("fr.tsv", sep="\t", encoding="utf-8")

# Create mapping between m2m-aligner input and original data
df_map = {(row.prepared_text, row.simple_pronunciation): i for i, row in df.iterrows()}

# Load aligned dataset
aligned_df = pd.read_csv("train.out.tsv", sep="\t", encoding="utf-8", header=None)

# Process aligned pairs
middle_indices = set()
results = []
for _, row in tqdm(aligned_df.iterrows(), total=len(aligned_df)):

    # Find original entry
    key = (
        row[0].replace("|", " ").replace(":", " ").rstrip(),
        row[1].replace("|", " ").replace(":", " ").replace("... ", "").rstrip(),
    )
    index = df_map.get(key)
    #assert index is not None
    if index is None:
        continue
    true_row = df.iloc[index]

    # Use original text as reference, to keep case information
    true_text = true_row.text
    offset = 0

    # Use original pronunciation to get syllable markers
    target = true_row.prepared_pronunciation.split(" ")
    target.reverse()

    # Use alignment to infer syllable marker location in text
    source = []
    for src, dst in zip(row[0].rstrip("|").split("|"), row[1].rstrip("|").split("|")):

        if dst != "...":
            
            # Iterate over phonemes in this aligned chunk pair
            for j, phoneme in enumerate(dst.split(":")):
                p = target.pop()

                # Check if there is a syllable marker at this location
                if p == ".":
                    source.append(".")

                    # Consume any duplicated dot...
                    while p == ".":
                        p = target.pop()
                    
                    # If alignment is good, syllable marker should not appear inside a chunk
                    if j > 0:
                        middle_indices.add(index)

                assert p == phoneme

        # Consume graphemes from original text
        count = len(src.replace(":", ""))
        true_src = true_text[offset:offset+count]
        assert src.replace(":", "").replace("_", " ") == true_src.lower()
        offset += count
        source.append(true_src)

    # Export result
    source = "".join(source)
    result = true_text, true_row.pronunciation, source
    results.append(result)

# Export as TSV
result_df = pd.DataFrame(results, columns=["text", "pronunciation", "syllable"])
result_df.to_csv("fr.syllable.tsv", sep="\t", encoding="utf-8", index=False)
