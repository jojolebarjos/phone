
# Many-to-Many Aligner

This experiment uses [m2m-aligner](https://github.com/letter-to-phoneme/m2m-aligner), the official implementation of:

_Applying Many-to-Many Alignments and Hidden Markov Models to Letter-to-Phoneme Conversion_, Sittichai, Jiampojamarn,  Grzegorz Kondrak and Tarek Sherif, 2007

The goal is to align graphemes and phonemes, in order to transfer syllable markers.

First, train model on subset:

```
./m2m-aligner --maxX 3 --maxY 1 --delX --nullChar ... -i train.tsv -o train.out.tsv --alignerOut fr.model
```

Then, apply on whole dataset:

```
./m2m-aligner --maxX 3 --delX --nullChar ... -i infer.tsv -o infer.out.tsv --alignerIn fr.model
```
