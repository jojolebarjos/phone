
def parse_sequences(line):
    line = line.rstrip()
    source, target = line.split(" ||| ")
    source = source.split()
    target = target.split()
    return source, target


def parse_alignment(line):
    line = line.rstrip()
    pairs = []
    for part in line.split():
        x, y = part.split("-")
        x = int(x)
        y = int(y)
        pair = x, y
        pairs.append(pair)
    return pairs


def align(source, target, alignment):
    aligned_source = []
    aligned_target = []
    i = 0
    j = 0
    for x, y in alignment:
        s = []
        t = []
        while i <= x:
            s.append(source[i])
            i += 1
        while j <= y:
            t.append(target[j])
            j += 1
        s = "".join(s)
        t = "".join(t)
        aligned_source.append(s)
        aligned_target.append(t)
    return aligned_source, aligned_target
        

i = 0
with open("tmp.txt", "r", encoding="utf-8") as sequence_file:
    with open("forward.align", "r") as pair_file:
        with open("aligned.txt", "w", encoding="utf-8", newline="\n") as output_file:
            for sequence_line, pair_line in zip(sequence_file, pair_file):
                source, target = parse_sequences(sequence_line)
                alignment = parse_alignment(pair_line)
                aligned_source, aligned_target = align(source, target, alignment)
                output_file.write("\t".join(aligned_source) + "\n")
                output_file.write("\t".join(aligned_target) + "\n")
                output_file.write("\n")
                i += 1
                if i >= 100:
                    break
