
import pandas as pd

import regex as re

import csv


df = pd.read_csv("../../data/wiktionary/fr.train.tsv", sep="\t", na_filter=False)


def prepare_text(text):
    text = text.lower()
    text = text.replace(" ", "_")
    text = " ".join(text)
    return text


BLANK_R = re.compile(r"[ \u203f]")
STRIP_R = re.compile(r"[\.\(\)\u0361]")
IPA_R = re.compile(r"(.\u0303?\u02d0?)*")

def prepare_pronunciation(pronunciation):
    pronunciation = BLANK_R.sub("_", pronunciation)
    pronunciation = STRIP_R.sub("", pronunciation)
    match = IPA_R.fullmatch(pronunciation)
    pronunciation = " ".join(match.captures(1))
    return pronunciation


df["text"] = df["text"].apply(prepare_text)
df["pronunciation"] = df["pronunciation"].apply(prepare_pronunciation)

df = df.sample(frac=1)

with open("tmp.txt", "w", encoding="utf-8", newline="\n") as file:
    for _, row in df.iterrows():
        file.write(f"{row.text} ||| {row.pronunciation}\n")
