
# Align graphemes and phonemes

```
python convert.py
```

```
docker build -t fast_align .
docker run -ti -v "$(pwd):/shared" --name fast_align fast_align

./fast_align -i /shared/tmp.txt -d -o -v > /shared/forward.align
```

```
python pad.py
```


## Links

 * [You May Not Need Attention](https://arxiv.org/abs/1810.13409)
 * [fast_align](https://github.com/clab/fast_align)
