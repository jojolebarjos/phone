
# Experiments

This folder contains various experiments using this dataset.

 * Grapheme and phoneme alignment
    * [using m2m-aligner](./m2m-aligner/)
    * [using fast_align](./fast_align/)
 * Grapheme to phoneme (i.e. seq2seq)
    * [using monotonic attention](./scratch/)
    * [using AllenNLP (WIP)](./allennlp/)
