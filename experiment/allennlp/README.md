
# AllenNLP Experiment

_At the time of writing, version 1.0 is still under heavy development, so this experiment is locked to 0.9_

Install [PyTorch](https://pytorch.org/) and [AllenNLP](https://allennlp.org/):

```
pip install "allenlp<1.0"
```

Train the model using:

```
allennlp train --include-package custom -s model config.json
```
