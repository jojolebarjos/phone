
from typing import Dict, Iterable, Optional

import io
import logging

from overrides import overrides

from allennlp.common.file_utils import cached_path
from allennlp.common.util import START_SYMBOL, END_SYMBOL
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import TextField
from allennlp.data.instance import Instance
from allennlp.data.tokenizers import Token, Tokenizer, WordTokenizer
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer


logger = logging.getLogger(__name__)


@DatasetReader.register("tsv")
class TSVDatasetReader(DatasetReader):
    def __init__(
        self,
        source_tokenizer: Tokenizer = None,
        target_tokenizer: Tokenizer = None,
        source_token_indexers: Dict[str, TokenIndexer] = None,
        target_token_indexers: Dict[str, TokenIndexer] = None,
        source_add_start_token: bool = True,
        source_max_tokens: Optional[int] = None,
        target_max_tokens: Optional[int] = None,
        has_header: bool = True,
        lazy: bool = False,
    ):
        super().__init__(lazy)
        
        self._source_tokenizer = source_tokenizer or WordTokenizer()
        self._target_tokenizer = target_tokenizer or self._source_tokenizer
        self._source_token_indexers = source_token_indexers or {"tokens": SingleIdTokenIndexer()}
        self._target_token_indexers = target_token_indexers or self._source_token_indexers
        self._source_add_start_token = source_add_start_token
        self._source_max_tokens = source_max_tokens
        self._target_max_tokens = target_max_tokens
        self._has_header = has_header
    
    def _read(self, file_path: str) -> Iterable[Instance]:
        with io.open(cached_path(file_path), "r", encoding="utf-8", newline="\n") as file:
            logger.info("Reading instances from lines in file at: %s", file_path)
            file = iter(file)
            if self._has_header:
                next(file, None)
            for line in file:
                line = line.rstrip("\r\n")
                parts = line.split("\t")
                if len(parts) < 2:
                    parts.append(None)
                source, target = parts
                yield self.text_to_instance(source, target)
    
    @overrides
    def text_to_instance(self, source: str, target: str = None):
        
        tokenized_source = self._source_tokenizer.tokenize(source)
        if self._source_max_tokens and len(tokenized_source) > self._source_max_tokens:
            tokenized_source = tokenized_source[:self._source_max_tokens]
        if self._source_add_start_token:
            tokenized_source.insert(0, Token(START_SYMBOL))
        tokenized_source.append(Token(END_SYMBOL))
        source_field = TextField(tokenized_source, self._source_token_indexers)
        
        if target is None:
            return Instance({"source_tokens": source_field})
        
        tokenized_target = self._target_tokenizer.tokenize(target)
        if self._target_max_tokens and len(tokenized_target) > self._target_max_tokens:
            tokenized_target = tokenized_target[:self._target_max_tokens]
        tokenized_target.insert(0, Token(START_SYMBOL))
        tokenized_target.append(Token(END_SYMBOL))
        target_field = TextField(tokenized_target, self._target_token_indexers)
        return Instance({"source_tokens": source_field, "target_tokens": target_field})
