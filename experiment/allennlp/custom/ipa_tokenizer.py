
from typing import List

from overrides import overrides

import regex as re

from allennlp.data.tokenizers.token import Token
from allennlp.data.tokenizers.tokenizer import Tokenizer


IPA_R = re.compile(r"(.\u0303?)*")


@Tokenizer.register("ipa")
class IPATokenizer(Tokenizer):
    """A specialized tokenizer for IPA characters.

    """

    @overrides
    def tokenize(self, text: str) -> List[Token]:
        match = IPA_R.fullmatch(text)
        return [Token(t) for t in match.captures(1)]
