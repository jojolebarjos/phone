
# Sequence-to-Sequence from Scratch

...


## Links

 * Connectionist Temporal Classification:
   * [Connectionist Temporal Classification: Labelling Unsegmented Sequence Data with Recurrent Neural Networks](https://www.cs.toronto.edu/~graves/icml_2006.pdf)
   * [Self-Attention Networks for Connectionist Temporal Classification in Speech Recognition](https://arxiv.org/abs/1901.10055)
 * ...
   * [Sequence Transduction with Recurrent Neural Networks](https://arxiv.org/abs/1211.3711)
   * [Speech Recognition with Deep Recurrent Neural Networks](https://arxiv.org/abs/1303.5778)
 * "Block-wise" neural transducer, which requires an approximation of alignments:
   * [A Neural Transducer](https://arxiv.org/abs/1511.04868)
   * [Improving the Performance of Online Neural Transducer Models](https://arxiv.org/abs/1712.01807)
 * Monotonic attention
   * [Online and Linear-Time Attention by Enforcing Monotonic Alignments](https://arxiv.org/abs/1704.00784)
   * [Monotonic Chunkwise Attention](https://arxiv.org/abs/1712.05382) ([code](https://github.com/craffel/mocha))
   * [Revisiting Character-Based Neural Machine Translation with Capacity and Compression](https://arxiv.org/abs/1808.09943)
   * [An Online Attention-based Model for Speech Recognition](https://arxiv.org/abs/1811.05247)
   * [Exact Hard Monotonic Attention for Character-Level Transduction](https://arxiv.org/abs/1905.06319)
