
from copy import copy

import torch


class Input:
    def __init__(self):
        self.value = None
        self._next = None
        self._iter = None
        
    @property
    def next(self):

        # Check to see if result is cached
        if self._iter is None:
            return self._next

        # Create new node
        input = Input()
        input.value = next(self._iter, None)

        # If feed is exhausted, disconnect
        if input.value is not None:
            input._iter = self._iter

        # Update our state
        self._next = input
        self._iter = None
        return self._next

    def __iter__(self):
        current = self
        while True:
            if current.value is None:
                return
            yield current.value
            current = current.next

    @staticmethod
    def from_iterable(iterable):
        input = Input()
        input._iter = iter(iterable)
        input.value = next(input._iter, None)
        if input.value is None:
            input._iter = None
            input._next = None
        return input

    def __repr__(self):
        parts = []
        current = self
        while current.value is not None:
            parts.append(repr(current.value))
            if current._iter is not None or current._next is None:
                break
            current = current._next
        if current._iter is not None:
            parts.append("...")
        return "[" + ", ".join(parts) + "]"


class _State:

    def init(self):
        with torch.no_grad():

            # Global parameters
            self.device = self.model.source_encoder.embedding.weight.device

            # Select first input
            self.input_offset = 0

            # Initialize encoder
            self.encoder_state = None

            # Initialize decoder
            self.decoder_state = model.source_decoder_cell.init_state(1)
            self.previous_output = 1
            self.output_offset = 0

    def move(self):

        assert self.input.next is not None
        self.input = self.input.next
        self.input_offset += 1

    def emit(self, output):

        # Update output
        self.previous_output = output

    def encode(self):
        with torch.no_grad():

            # Get current input as tensor
            input = torch.tensor(self.input.value, dtype=torch.int64, device=self.device).reshape(1, 1)
            length = torch.tensor([1])
            
            # Encode step
            self.memory, self.encoder_state = self.model.source_encoder(input, length, self.encoder_state)

    def compute_emission_probability(self):
        with torch.no_grad():

            # Compute attention probability
            energy = self.model.source_energy(self.decoder_state, self.memory)
            keep_probability = torch.sigmoid(energy)
            self.keep_probability = float(keep_probability[0, 0].cpu())

    def compute_output_probability(self):
        with torch.no_grad():

            # Apply decoder cell
            context = self.memory[0]
            previous_output = torch.tensor([self.previous_output], dtype=torch.int64, device=self.device)
            previous_embedded_output = self.model.target_encoder.embedding(previous_output)
            output_logits, self.decoder_state = model.source_decoder_cell(self.decoder_state, context, previous_embedded_output)
            output_probabilities = torch.softmax(output_logits[0], dim=0)
            self.output_probabilities = output_probabilities.cpu()


class Step:
    def __init__(self):
        self._move_state = None
        self._output_state = None

    @property
    def keep_probability(self):
        return self._move_state.keep_probability

    @property
    def output_probabilities(self):
        return self._output_state.output_probabilities

    @staticmethod
    def create(model, input):
        if not isinstance(input, Input):
            input = Input.from_iterable(input)
        move_state = _State()
        move_state.model = model
        move_state.input = input
        move_state.init()
        move_state.encode()
        return Step._finalize(move_state)

    @staticmethod
    def _finalize(move_state):
        move_state.compute_emission_probability()
        output_state = copy(move_state)
        output_state.compute_output_probability()
        step = Step()
        step._move_state = move_state
        step._output_state = output_state
        return step

    def move(self):
        move_state = copy(self._move_state)
        move_state.move()
        move_state.encode()
        return self._finalize(move_state)

    def emit(self, output):
        move_state = copy(self._output_state)
        move_state.emit(output)
        return Step._finalize(move_state)

    @property
    def choices(self):
        k = self.keep_probability
        os = self.output_probabilities
        result = {i: k * float(o) for i, o in enumerate(os)}
        result[None] = 1.0 - k
        return result

    def next(self):
        for output, probability in sorted(self.choices.items(), key=lambda p: -p[1]):
            if output is None:
                step = self.move()
            else:
                step = self.emit(output)
            yield output, probability, step
