
import io

import regex as re

import torch


def tokenize_text(text):
    return list(text)


IPA_R = re.compile(r"(.\u0303?)*")

def tokenize_ipa(ipa):
    match = IPA_R.fullmatch(ipa)
    return match.captures(1)


class Vocabulary:
    def __init__(self, tokens):
        tokens = sorted(set(tokens))
        self.pad_index = 0
        self.end_index = 1
        self.tokens = ["<PAD>", "<END>", *tokens]
        self.token_map = {token: index + 2 for index, token in enumerate(tokens)}

    def __len__(self):
        return len(self.tokens)

    def encode(self, tokens):
        indices = []
        for token in tokens:
            index = self.token_map[token]
            indices.append(index)
        indices.append(self.end_index)
        return indices
    
    def decode(self, indices):
        tokens = []
        for i in indices:
            if i == 0:
                break
            token = self.tokens[i]
            tokens.append(token)
        return tokens

    def save(self, path):
        with io.open(path, "w", encoding="utf-8", newline="\n") as file:
            for token in self.tokens[2:]:
                file.write(f"{token}\n")

    @classmethod
    def load(cls, path):
        tokens = []
        with io.open(path, "r", encoding="utf-8", newline="\n") as file:
            for line in file:
                tokens.append(line.rstrip("\n"))
        return cls(tokens)


def pack_indices(indices, max_length=None):
    n = len(indices)
    length = max(len(i) for i in indices)
    if max_length is not None and length > max_length:
        length = max_length
    values = torch.zeros((length, n), dtype=torch.long)
    lengths = torch.zeros(n, dtype=torch.long)
    for i in range(n):
        l = len(indices[i])
        l = min(l, length)
        for j in range(l):
            values[j, i] = indices[i][j]
        lengths[i] = l
    return values, lengths
