
import torch
import torch.nn as nn
import torch.nn.functional as F


class Encoder(nn.Module):
    def __init__(self, num_symbol, embedding_size, output_size):
        super().__init__()
        
        # Store parameters
        self.num_symbol = num_symbol
        self.embedding_size = embedding_size
        self.output_size = output_size
        
        # Symbols are embedded
        self.embedding = nn.Embedding(
            num_embeddings=num_symbol,
            embedding_dim=embedding_size,
            padding_idx=0,
        )
        
        # Use a single GRU
        self.gru = nn.GRU(
            input_size=embedding_size,
            hidden_size=output_size,
            num_layers=1,
            dropout=0,
            bidirectional=False,
        )

    def forward(self, indices, lengths, state=None):
        embeddings = self.embedding(indices)
        packed_embeddings = nn.utils.rnn.pack_padded_sequence(embeddings, lengths, enforce_sorted=False)
        packed_outputs, state = self.gru(packed_embeddings, state)
        outputs, _ = nn.utils.rnn.pad_packed_sequence(packed_outputs)
        return outputs, state


def exclusive_stable_cumprod(x):
    # x: seq_len x batch_size
    
    # In log-space, for stability
    x = torch.log(x)
    x = torch.cumsum(x, dim=0)
    x = torch.exp(x)
    
    # Exclude first step
    ones = torch.ones(1, x.shape[1], dtype=x.dtype, device=x.device)
    x = torch.cat([ones, x[:-1, :]], dim=0)
    
    return x


def compute_alpha(probability, previous_alpha):
    # probability: seq_len x batch_size
    # previous_alpha: seq_len x batch_size
    
    # TODO will need to handle input_length?
    # TODO authors strongly suggest to use gradient clipping during alpha calculation
    # Also, see Appendix G for numerical considerations
    
    cumprod_one_minus_probability = exclusive_stable_cumprod(1.0 - probability)
    
    # Authors suggest to keep denominator values in [epsilon, 1]
    #   Note: as an alternative, simply setting denominator to 1 seems to produce good results, according to authors
    denominator = cumprod_one_minus_probability
    denominator = torch.clamp(denominator, 1e-10, 1.0)
    
    # See Appendix C.1 for recurrence relation expansion
    alpha = probability * cumprod_one_minus_probability * torch.cumsum(previous_alpha / denominator, dim=0)
    return alpha


class Energy(nn.Module):
    
    def __init__(self, memory_size, decoder_state_size, attention_size, initial_r=-4):
        super().__init__()
        
        # Store parameters
        self.memory_size = memory_size
        self.decoder_state_size = decoder_state_size
        self.attention_size = attention_size
        
        # Linear combination of memory and state
        self.W = nn.Linear(decoder_state_size, attention_size, bias=False)
        self.V = nn.Linear(memory_size, attention_size, bias=False)
        self.b = nn.Parameter(torch.Tensor(attention_size).normal_())
        
        # Authors use weight normalization to stabilize energy scale
        self.v = nn.utils.weight_norm(nn.Linear(attention_size, 1))
        self.v.weight_g.data = torch.Tensor([[1 / attention_size]]).sqrt()
        
        # Authors add a bias, since softmax is not invariant to offset
        self.r = nn.Parameter(torch.tensor(initial_r, dtype=torch.float32))
    
    # Compute attention score given an memory sequence and the current decoder state
    def forward(self, state, memory):
        # state: batch_size x decoder_state_size
        # memory: seq_len x batch_size x memory_size
        projected_state = self.W(state)[None, :, :]
        projected_memory = self.V(memory)
        energy = torch.tanh(projected_state + projected_memory + self.b)
        energy = self.v(energy).squeeze(2) + self.r
        return energy
        # energy: seq_len x batch_size


class DecoderCell(nn.Module):
    def __init__(self, decoder_state_size, memory_size, output_embedding_size, num_output_symbols):
        super().__init__()
        
        # Store parameters
        self.decoder_state_size = decoder_state_size
        self.memory_size = memory_size
        self.output_embedding_size = output_embedding_size
        self.num_output_symbols = num_output_symbols
        
        # Use a single layer GRU, taking a linear combination of state and previous input
        self.state_cell = nn.GRUCell(
            input_size = memory_size + output_embedding_size,
            hidden_size = decoder_state_size
        )
        
        # Add a linear classifier on top of GRU state
        self.classifier = nn.Linear(
            in_features = decoder_state_size,
            out_features = num_output_symbols
        )
    
    def forward(self, decoder_state, context, previous_embedded_output):
        # decoder_state: seq_len x batch_size x decoder_state_size (float)
        # context: batch_size x memory_size
        # previous_embedded_output: batch_size x output_embedding_size (float)
        
        pack = torch.cat([context, previous_embedded_output], dim=1)
        # TODO maybe apply fully connected on this packed vector
        
        next_decoder_state = self.state_cell(pack, decoder_state)
        
        output_logits = self.classifier(next_decoder_state)
        
        return output_logits, next_decoder_state

    def init_state(self, batch_size):
        # TODO maybe learn this one
        return torch.zeros(batch_size, self.decoder_state_size, dtype=torch.float32, device=self.state_cell.bias_hh.device)


def masked_cross_entropy(logits, target, mask):
    # logits: d_1 x ... x d_n x C
    # target: d_1 x ... x d_n
    # mask: d_1 x ... x d_n
    
    # Flatten tensors
    num_classes = logits.shape[-1]
    logits = logits.reshape(-1, num_classes)
    target = target.reshape(-1)
    mask = mask.reshape(-1)
    
    # Compute masked categorical loss
    losses = F.cross_entropy(logits, target, reduction='none') * mask
    loss = losses.sum() / mask.sum()
    return loss


class MonotonicModel(nn.Module):
    def __init__(
        self,
        num_source_symbols,
        num_target_symbols,
        embedding_size,
        memory_size,
        state_size,
        attention_size,
    ):
        super().__init__()
        
        # Store parameters
        self.num_source_symbols = num_source_symbols
        self.num_target_symbols = num_target_symbols
        self.embedding_size = embedding_size
        self.memory_size = memory_size
        self.state_size = state_size
        self.attention_size = attention_size
        
        # Encoders
        self.source_encoder = Encoder(num_source_symbols, embedding_size, memory_size)
        self.target_encoder = Encoder(num_target_symbols, embedding_size, memory_size)
        
        # Energies
        self.source_energy = Energy(memory_size, state_size, attention_size)
        self.target_energy = Energy(memory_size, state_size, attention_size)
        
        # Decoder cells
        self.source_decoder_cell = DecoderCell(state_size, memory_size, embedding_size, num_target_symbols)
        self.target_decoder_cell = DecoderCell(state_size, memory_size, embedding_size, num_source_symbols)

    def forward(
        self,
        source_indices,
        source_lengths,
        target_indices,
        target_lengths,
    ):
        
        # Source to target
        s2t_loss = self._forward(
            self.source_encoder,
            self.source_energy,
            self.source_decoder_cell,
            self.target_encoder.embedding,
            source_indices,
            source_lengths,
            target_indices,
            target_lengths,
        )
        
        # Target to source
        t2s_loss = self._forward(
            self.target_encoder,
            self.target_energy,
            self.target_decoder_cell,
            self.source_encoder.embedding,
            target_indices,
            target_lengths,
            source_indices,
            source_lengths,
        )
        
        return s2t_loss + t2s_loss

    def _forward(
        self,
        encoder,
        energy_fn,
        decoder_cell,
        output_embedding,
        input_indices,
        input_lengths,
        output_indices,
        output_lengths,
    ):
        
        device = input_indices.device
        max_input_length, batch_size = input_indices.shape
        max_output_length, _ = output_indices.shape
        
        # Encode input
        memory, _ = encoder(input_indices, input_lengths)
        
        # Begin with full attention on start symbol
        previous_alpha = torch.zeros(max_input_length, batch_size, dtype=torch.float32, device=device)
        previous_alpha[0, :] = 1.0
        
        # Begin with <END> as previous output, as a dummy start marker
        previous_output = torch.ones(batch_size, dtype=torch.int64, device=device)
        
        # Initialize decoder
        decoder_state = decoder_cell.init_state(batch_size)
        
        # Iterate through output steps
        all_output_logits = []
        for output_step in range(max_output_length):
            
            # Compute energy on whole memory
            energy = energy_fn(decoder_state, memory)
            
            # Add noise during training to encourage discreteness
            if self.training:
                energy += torch.randn_like(energy)
            
            # Compute probability of choosing each memory step
            probability = torch.sigmoid(energy)
            
            # Compute attention probability
            alpha = compute_alpha(probability, previous_alpha)
            
            # Compute context
            context = (memory * alpha[:, :, None]).sum(dim=0)
            
            # Apply decoder cell
            previous_embedded_output = output_embedding(previous_output)
            output_logits, decoder_state = decoder_cell(decoder_state, context, previous_embedded_output)
            all_output_logits.append(output_logits)

            # Select output
            # TODO adjust teacher probability
            if not self.training or torch.rand([]) < 0.5:
                previous_output = output_logits.argmax(dim=1)
            else:
                previous_output = output_indices[output_step]
        
        # Compute loss, ignoring padding
        all_output_logits = torch.stack(all_output_logits)
        mask = torch.arange(max_output_length, device=device)[:, None] < output_lengths[None, :]
        mask = mask.to(torch.float32)
        loss = masked_cross_entropy(all_output_logits, output_indices, mask)
        return loss
