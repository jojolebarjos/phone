
import torch


def infinite_range():
    i = 0
    while True:
        yield i
        i += 1


def maybe_range(n):
    if n is None:
        return infinite_range()
    return range(n)


def transduce(model, inputs, max_output_per_input=10):

    device = model.source_encoder.embedding.weight.device
    
    # Initialize encoder and decoder
    encoder_state = None
    decoder_state = model.source_decoder_cell.init_state(1)
    
    # Begin with <END> of sentence symbol, as it is used begin marker
    previous_output = 1
    
    # Stream input
    pos = 0
    for input in inputs:
        input = torch.tensor(input, dtype=torch.int64, device=device).reshape(1, 1)
        length = torch.tensor([1])
        
        # Encode step
        memory, encoder_state = model.source_encoder(input, length, encoder_state)
        
        # Keep this input step as long as desired
        for _ in maybe_range(max_output_per_input):
            
            # Compute attention probability
            energy = model.source_energy(decoder_state, memory)
            probability = torch.sigmoid(energy)
            
            # Choose whether this step should be attended
            # TODO make this random?
            keep = probability >= 0.5
            if not keep:
                break
            
            # Apply decoder cell
            context = memory[0]
            previous_embedded_output = model.target_encoder.embedding(torch.tensor([previous_output], dtype=torch.long, device=device))
            output_logits, decoder_state = model.source_decoder_cell(decoder_state, context, previous_embedded_output)
            
            # Choose output symbol
            # TODO make this random? temperature?
            output = int(output_logits.argmax(dim=1).squeeze(0))
            
            # Yield (or terminate on end of sequence)
            if output == 1:
                return
            yield pos, output
            previous_output = output
        
        # Handle overflow
        else:
            # TODO
            pass
        
        pos += 1
    
    # Feed zeroes until end of sequence is generated
    context = torch.zeros(1, model.memory_size, dtype=torch.float32, device=device)
    for _ in maybe_range(max_output_per_input):
        
        # Apply decoder cell
        previous_embedded_output = model.target_encoder.embedding(torch.tensor([previous_output], dtype=torch.long, device=device))
        output_logits, decoder_state = model.source_decoder_cell(decoder_state, context, previous_embedded_output)
        
        # Choose output symbol
        # TODO make this random? temperature?
        output = int(output_logits.argmax(dim=1).squeeze(0))
        
        # Yield (or terminate on end of sequence)
        if output == 1:
            return
        yield pos, output
        previous_output = output
    
    # Handle overflow
    else:
        # TODO
        pass
